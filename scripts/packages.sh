#!/bin/bash

HELP="Usage: sudo ./packages.sh <operation> [...]
Automatically install Arch Linux packages

operations:
    -a           installs AUR packages
    -t <options> installs packages of GUI or CLI

Examples:
$ sudo ./packages.sh -at GUI
$ sudo ./packages.sh -a -t cli"

source $(dirname "$0")/shared.sh

if [ -z "$SUDO_USER" ]; then
    echo "$HELP"
    exit 1
fi

BASE_PACKAGES=(
    'zsh'
    'zsh-theme-powerlevel10k'
    'acpi'
    'atool'
    'bat'
    'blueman'
    'bluez-utils'
    'curl'
    'dhcpcd'
    'rxvt-unicode' 
    'exa'
    'fzf'
    'ffmpegthumbnailer'
    'firewalld'
    'git'
    'go-pie'
    'go-tools'
    'highlight'
    'htop'
    'jq'
    'less'
    'lsd'
    'mediainfo'
    'neofetch'
    'odt2txt'
    'openvpn'
    'openssh'
    'pacman-contrib'
    'poppler'
    'python-pip'
    'ranger'
    'tmux'
    'usbguard'
    'weechat'
    'whois'
    'rofi'
    'xorg-xinit'
    'xf86-video-nouveau'
    'xorg-server'
    'xorg'
    'xorg-xrandr'
    'xorg-xbacklight'
    'xbindkeys'
    'feh'
    'lxappearance'
    'lightdm'
    'lightdm-gtk-greeter'
    'xorg-xprop'
    'dmenu'
    'dunst'
    'xautolock'
    'lm_sensors'
    'xclip'
    'unclutter'
    'virtualbox-host-modules-arch'
    'acpi'
    'reflector'
    'playerctl'
    'pulseaudio'
    'pulseaudio-alsa'
    'pulseaudio-bluetooth'
    'xfce4-power-manager'
)

PACMAN_PACKAGES=(
    'i3-gaps'
    'i3blocks'
    'i3lock'

    'adapta-gtk-theme'
    'papirus-icon-theme'
    'nautilus' 
    'arandr'
    'discount'
    'firefox'
    'npm'
    'xfce4-notifyd'
    'xfce4-screenshooter'
    'elinks'
    'gimp'
    'pavucontrol'
    'vlc'
                   
)

AUR_PACKAGES=(
    'oh-my-zsh-git'
    'gotop'
    'rcm'
    'picom-tryone-git'
    'w3m-imgcat'
    'polybar'
    'virtualbox-guest-dkms-vmsvga'
)

function installZshCustomPlugins() {
    banner "I will install Oh My Zsh and plugins"
    sudo -u "$SUDO_USER" -- bash -c "
    git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
    git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions"
}

function installPolybarThemes() {
    sudo -u "$SUDO_USER" -- sh -c "
    git clone --depth=1 https://github.com/adi1090x/polybar-themes.git .config/polybar/polybar-themes
    chmod +x ../.config/polybar/polybar-themes/setup.sh
    bash ../.config/polybar/polybar-themes/setup.sh
    cd - &>/dev/null"
}

function installAurPackages() {
    banner "I will install the AUR packages"
    packageIterator "yay" "${AUR_PACKAGES[@]}"
}

function installYay() {
    sudo -u "$SUDO_USER" -- sh -c "
    git clone https://aur.archlinux.org/yay.git;
    cd yay || return;
    yes | makepkg -si"
}

function configurePacman() {
    sed -i 's/#Color/Color\nILoveCandy/g' /etc/pacman.conf
    #sed -i 's/#TotalDownload/TotalDownload/g' /etc/pacman.conf
}

function installPacmanPackages() {
    banner "I will install the base system"
    configurePacman
    packageIterator "pacman" "${BASE_PACKAGES[@]}"

    banner "I will install the CLI packages"
    packageIterator "pacman" "${PACMAN_PACKAGES[@]}"

    installYay
}

#base16-manager install chriskempson/base16-vim
#base16-manager install 0xdec/base16-rofi
installPacmanPackages
installAurPackages
installZshCustomPlugins
# installPolybarThemes

banner "Done :)"
