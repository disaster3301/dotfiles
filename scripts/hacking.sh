#!/bin/bash

sudo pacman -Suuuy --noconfirm
source $(dirname "$0")/shared.sh

RECON=(
  'nmap' # Port scanner.
)

WEBTOOLS=(
  'burpsuite' # Web proxy.
)

OSINT=(
  'osi.ig' # Instagram OSINT Tool gets a range of information from an Instagram account.
  'instagramosint' # An Instagram Open Source Intelligence Tool.
  'ultimate-facebook-scraper' # A bot which scrapes almost everything about a Facebook user's profile.
  'fbi' # An accurate facebook account information gathering.
  'facebot' # A facebook profile and reconnaissance system.
)

function installBlackArchrepo() {
    cd ~/Downloads
    curl -O https://blackarch.org/strap.sh
    chmod +x strap.sh
    sudo ./strap.sh
    sudo pacman -Suuuy --noconfirm
}

function installPackages() {
    banner "Install recon packages."
    packageIterator "pacman" "${RECON[@]}"

    banner "Install osint packages."
    packageIterator "pacman" "${OSINT[@]}"

    banner "Install web pentest packages."
    packageIterator "pacman" "${WEBTOOLS[@]}"
}

installBlackArchrepo
installPackages

banner "Done :)"
