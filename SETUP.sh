#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    echo "DONT START AS ROOT"
    exit 1
fi

HACKING=false
VBOX=false

git pull
sudo pacman -Suuuy --noconfirm
sudo pacman -S zsh --noconfirm
sudo ./scripts/users.sh -rc anon
sudo usermod -s $(which zsh) anon
sudo ./scripts/packages.sh -a -t cli
sudo ./scripts/files-imports.sh

# Hacking packages.
if [ "$HACKING" = true ] ; then
    sudo ./scripts/hacking.sh
fi

# Install dotfiles.
env RCRC=$HOME/dotfiles/rcrc rcup -f

# Update locate DB.
sudo updatedb

# Add i3blocks modules
git clone https://github.com/vivien/i3blocks-contrib.git ~/.i3/i3scripts/

# Reboot.
sudo reboot
